package internal

import (
	"fmt"
	"os"
)

const (
	// XUserToken ...
	XUserToken = "X-User-Token"
	//XGoogleAccessToken ...
	XGoogleAccessToken = "X-Google-Access-Token"
	// GoogleClientID ...
	GoogleClientID = "xxxxxxxxxxxx.apps.googleusercontent.com"
	// GoogleMapAPIKey ...
	GoogleMapAPIKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
	// GoogleClientSecret ...
	GoogleClientSecret = "xxxxxxxxxxxxxxxxxxx"
	// MysqlConnectionString ... <user>:<password>@/<database>
	MysqlConnectionString = "root:121212@/userapp"
)

var (
	// Port ...
	Port = ":" + os.Getenv("PORT")
	// WebURL ...
	WebURL = fmt.Sprintf("http://127.0.0.1%s", Port)
	// API ...
	API = fmt.Sprintf("http://127.0.0.1%s/v1", Port)
)
