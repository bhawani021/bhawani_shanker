## Install

1. You must have Golang installed
2. You must have MySQL installed
2. You must have set $GOPATH
3. You must have src, bin, pkg under $GOPATH
4. Run following command 

```
$ go get bitbucket.org/bhawani021/bhawani_shanker
```
## Insatall dependencies

```
$ go get github.com/gorilla/mux
$ go get golang.org/x/oauth2
$ go get golang.org/x/oauth2/google
$ go get golang.org/x/crypto/bcrypt
$ go get github.com/Sirupsen/logrus
$ go get github.com/go-sql-driver/
```

### Config change
Open config.go file under  DIR path  $GOPATH/src/bitbucket.org/bhawani021/bhawani_shanker/internal and change following values  

```
GoogleClientID = "XXXXXXXXXXXX.apps.googleusercontent.com"
GoogleClientSecret = "xxxxxxxxxxxxxxxxxxx"
GoogleMapAPIKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
MysqlConnectionString = "user:password@tcp(host:port)/db"
```

### Mysql table structure
```
--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS user (
			id int(11) NOT NULL AUTO_INCREMENT, 
			name varchar(100) DEFAULT NULL,
			email varchar(200) NOT NULL,
			password varchar(200) NOT NULL,
			address varchar(500) DEFAULT NULL,
			telephone varchar(20) DEFAULT NULL,
			token varchar(32) NOT NULL,
			expiry datetime DEFAULT NULL,
			created datetime DEFAULT NULL,
			modified datetime DEFAULT NULL,
			PRIMARY KEY (id)
		) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--
```

## Run server
cd to $GOPATH/src/bhawani021/bhawani_shanker and run below command. 
Note: MySQL server must be running
```
$ go run server.go
```

### Tasks implemented
1. Login/Signup using form
2. Login/Signup using Google API
3. Logout
4. Profile page
5. Edit profile
6. Change password
7. Autocomplete address field using Google map API

### Pending
1. Forgot password page
