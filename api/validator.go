package api

import (
	"errors"
	"regexp"
	"strings"

	"bitbucket.org/bhawani021/bhawani_shanker/models"
)

func validateEmail(email string) bool {
	re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return re.MatchString(email)
}

// ValidateRequestPayload for login and signup call ...
func ValidateRequestPayload(u models.User) []error {
	allErrors := make([]error, 0)

	email := strings.TrimSpace(u.Email)
	password := u.Password

	// Validate email address
	if email == "" {
		allErrors = append(allErrors, errors.New("Please enter email address"))
	} else if !validateEmail(email) {
		allErrors = append(allErrors, errors.New("Please enter a valid email address"))
	}

	// Validate password
	if password == "" {
		allErrors = append(allErrors, errors.New("Please enter password"))
	}

	return allErrors
}

// ValidateEditProfilePayload for login and signup call ...
func ValidateEditProfilePayload(u models.UserProfile) []error {
	email := strings.TrimSpace(u.Email)
	allErrors := make([]error, 0)

	// Validate email address
	if email == "" {
		allErrors = append(allErrors, errors.New("Please enter email address"))
	} else if !validateEmail(email) {
		allErrors = append(allErrors, errors.New("Please enter a valid email address"))
	}
	return allErrors
}

// ValidateUpdatePasswordPayload ...
func ValidateUpdatePasswordPayload(pw models.UpdatePassword) []error {
	allErrors := make([]error, 0)
	password := pw.Password
	newPassword := pw.NewPassword

	// Validate current password
	if password == "" {
		allErrors = append(allErrors, errors.New("Please enter current password"))
	}

	// Validate new password
	if newPassword == "" {
		allErrors = append(allErrors, errors.New("Please enter new password"))
	}
	return allErrors
}
