package api

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"bitbucket.org/bhawani021/bhawani_shanker/internal"
	"bitbucket.org/bhawani021/bhawani_shanker/models"

	log "github.com/Sirupsen/logrus"
)

// UserGoogleAuth handler ...
func UserGoogleAuth(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("User Google Auth API.")

	// Read token
	accessToken := r.Header.Get(internal.XGoogleAccessToken)
	if accessToken == "" {
		errMsg := errors.New("Access token not found with request header")
		log.WithError(errMsg).Error("Access Token")
		Error(w, errMsg, http.StatusBadRequest)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken)
	if err != nil {
		log.WithError(err).Error(err)
		Error(w, err, http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	body, _ := ioutil.ReadAll(response.Body)

	// Unmarshal userinfo
	var profile models.GoogleUserInfo
	err = json.Unmarshal(body, &profile)
	if err != nil {
		log.WithError(err).Info("Unmarshal error.")
		Error(w, err, http.StatusBadRequest)
		return
	}

	if response.StatusCode == http.StatusOK {
		var user models.User
		email := profile.Email
		user.Email = email
		user.Name = profile.Name

		// Get token
		token := GenerateToken()
		encodedToken := EncodeToken(token)
		user.Token = token
		user.Expiry = GetTokenExpiryDate()

		// Check if user already exists
		exists, _ := user.CheckUserByEmail()
		if exists == true {
			err = user.Login(true)
			if err != nil {
				Error(w, err, http.StatusInternalServerError)
				return
			}
		} else {
			password := GeneratePassword()
			user.Password = password
			user.Address = ""
			user.Telephone = ""
			id, err := user.Save()
			if err != nil {
				Error(w, err, http.StatusInternalServerError)
				return
			}
			log.WithFields(log.Fields{"id": id}).Info("User saved successfully.")
		}
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set(internal.XUserToken, encodedToken)
		w.WriteHeader(http.StatusOK)
		w.Write(nil)

	}

	// Build API response
	b, err := json.Marshal(profile)
	if err != nil {
		log.WithError(err).Error("Error marshalling data.")
		Error(w, err, http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}
