package api

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	b64 "encoding/base64"

	"bitbucket.org/bhawani021/bhawani_shanker/models"
	log "github.com/Sirupsen/logrus"
)

// Error response for APIs ...
func Error(w http.ResponseWriter, err interface{}, code int) {
	w.Header().Set("Content-Type", "json/application; charset=utf-8")
	w.WriteHeader(code)
	var e interface{}
	errs := make([]string, 0)
	switch err.(type) {
	case error:
		er := err.(error).Error()
		errs = append(errs, er)
		e = errs
	case fmt.Stringer:
		er := err.(fmt.Stringer).String()
		errs = append(errs, er)
		e = errs
	case []error:
		errorList := err.([]error)
		for _, er := range errorList {
			errs = append(errs, er.(error).Error())
		}
		e = errs
	default:
		e = err
	}

	res := struct {
		Err interface{} `json:"error"`
	}{
		e,
	}

	b, err := json.Marshal(res)
	if err != nil {
		return
	}
	fmt.Fprintln(w, string(b))
}

// ReadBody reads request body ...
func ReadBody(r *http.Request) (models.User, error) {

	var user models.User

	// Read requeest body
	var buf []byte
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error while reading request body.")
		return user, err
	}

	// Unmashall data
	err = json.Unmarshal(buf, &user)
	if err != nil {
		log.WithError(err).Error("Error unmarshalling data.")
		return user, err
	}

	return user, nil
}

// GenerateToken ...
func GenerateToken() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

// GeneratePassword ...
func GeneratePassword() string {
	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

// EncodeToken ...
func EncodeToken(token string) string {
	encodedToken := b64.StdEncoding.EncodeToString([]byte(token))
	return encodedToken
}

// DecodeToken ...
func DecodeToken(token string) (string, error) {
	decodedToken, err := b64.StdEncoding.DecodeString(token)
	if err != nil {
		return "", err
	}
	return string(decodedToken), nil
}

// GetTokenExpiryDate ...
func GetTokenExpiryDate() time.Time {
	curent := time.Now()
	add := time.Hour * 24 * 7
	return curent.Add(add)
}
