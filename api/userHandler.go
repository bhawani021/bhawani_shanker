package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"bitbucket.org/bhawani021/bhawani_shanker/internal"
	"bitbucket.org/bhawani021/bhawani_shanker/models"

	log "github.com/Sirupsen/logrus"
)

// Login handler ...
func Login(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("Login call.")

	// Read request payload
	user, err := ReadBody(r)
	if err != nil {
		Error(w, err, http.StatusInternalServerError)
		return
	}

	// Validate request payload
	errs := ValidateRequestPayload(user)
	if len(errs) > 0 {
		log.WithFields(log.Fields{
			"email": user.Email,
			"error": errs,
		}).Error("Error while validating login payload.")
		Error(w, errs, http.StatusBadRequest)
		return
	}

	// Generate token
	token := GenerateToken()
	encodedToken := EncodeToken(token)
	user.Token = token

	// Token expiry date
	user.Expiry = GetTokenExpiryDate()

	// Authenticate user
	err = user.Login(false)
	if err != nil {
		Error(w, err, http.StatusInternalServerError)
		return
	}

	// Build API response
	response := struct {
		Email string `json:"email"`
	}{
		user.Email,
	}
	b, err := json.Marshal(response)
	if err != nil {
		log.WithError(err).Error("Error marshalling data.")
		Error(w, err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set(internal.XUserToken, encodedToken)
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

// Signup handler ...
func Signup(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("Signup call.")

	// Read request payload
	user, err := ReadBody(r)
	if err != nil {
		Error(w, err, http.StatusInternalServerError)
		return
	}

	// Validate request payload
	errs := ValidateRequestPayload(user)
	if len(errs) > 0 {
		log.WithFields(log.Fields{
			"email": user.Email,
			"error": errs,
		}).Error("Error while validating signup payload.")
		Error(w, errs, http.StatusBadRequest)
		return
	}

	// Validate if email already exists
	exists, err := user.CheckUserByEmail()
	if err != nil {
		Error(w, err, http.StatusBadRequest)
		return
	}
	if exists == true {
		Error(w, err, http.StatusBadRequest)
		return
	}

	// Generate token
	token := GenerateToken()
	encodedToken := EncodeToken(token)
	user.Token = token

	// Token expiry date
	user.Expiry = GetTokenExpiryDate()
	user.Name = ""
	user.Address = ""
	user.Telephone = ""

	// Save user into database
	id, err := user.Save()
	if err != nil {
		Error(w, err, http.StatusInternalServerError)
		return
	}
	log.WithFields(log.Fields{"id": id}).Info("User saved successfully.")

	// Build API response
	response := struct {
		ID    int64  `json:"user_id"`
		Email string `json:"email"`
	}{
		id,
		user.Email,
	}
	b, err := json.Marshal(response)
	if err != nil {
		log.WithError(err).Error("Error marshalling data.")
		Error(w, err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set(internal.XUserToken, encodedToken)
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

// Profile ...
func Profile(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("Profile API call")

	// Read token
	token := r.Header.Get(internal.XUserToken)
	decodedToken, err := DecodeToken(token)
	if err != nil {
		Error(w, "Invalid token provided", http.StatusUnauthorized)
		return
	}

	// Get user info using token
	var p models.UserProfile
	profile, err := p.GetUserProfileByToken(decodedToken)
	if err != nil {
		Error(w, err, http.StatusUnauthorized)
		return
	}

	// Build API response
	b, err := json.Marshal(profile)
	if err != nil {
		log.WithError(err).Error("Error marshalling data.")
		Error(w, err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

// EditProfile ...
func EditProfile(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("Edit profile API call")

	// Read token
	token := r.Header.Get(internal.XUserToken)
	decodedToken, err := DecodeToken(token)
	if err != nil {
		Error(w, "Invalid token provided", http.StatusUnauthorized)
		return
	}

	// Get user info using token
	var p models.UserProfile
	profile, err := p.GetUserProfileByToken(decodedToken)
	if err != nil {
		Error(w, err, http.StatusUnauthorized)
		return
	}

	// Read requeest body
	var buf []byte
	buf, err = ioutil.ReadAll(r.Body)
	if err != nil {
		Error(w, err, http.StatusUnauthorized)
		return
	}

	// Unmashall data
	var up models.UserProfile
	err = json.Unmarshal(buf, &up)
	if err != nil {
		log.WithError(err).Error("Error unmarshalling data.")
		Error(w, err, http.StatusBadRequest)
		return
	}

	// Validate request payload
	errs := ValidateEditProfilePayload(up)
	if len(errs) > 0 {
		log.WithFields(log.Fields{
			"email": up.Email,
			"error": errs,
		}).Error("Error while validating edit profile payload.")
		Error(w, errs, http.StatusBadRequest)
		return
	}
	up.ID = profile.ID

	err = up.EditProfile()
	if err != nil {
		log.WithError(err).Error("Update user profile.")
		Error(w, err, http.StatusInternalServerError)
		return
	}

	// Build API response
	b, err := json.Marshal(up)
	if err != nil {
		log.WithError(err).Error("Error marshalling data.")
		Error(w, err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

// ChangePassword ...
func ChangePassword(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("change password API call")

	// Read token
	token := r.Header.Get(internal.XUserToken)
	decodedToken, err := DecodeToken(token)
	if err != nil {
		Error(w, "Invalid token provided", http.StatusUnauthorized)
		return
	}

	// Get user info using token
	var p models.UserProfile
	profile, err := p.GetUserProfileByToken(decodedToken)
	if err != nil {
		Error(w, err, http.StatusUnauthorized)
		return
	}

	// Read request body
	var buf []byte
	buf, err = ioutil.ReadAll(r.Body)
	if err != nil {
		Error(w, err, http.StatusUnauthorized)
		return
	}

	// Unmashall data
	var up models.UpdatePassword
	err = json.Unmarshal(buf, &up)
	if err != nil {
		log.WithError(err).Error("Error unmarshalling data.")
		Error(w, err, http.StatusBadRequest)
		return
	}

	// Validate request payload
	errs := ValidateUpdatePasswordPayload(up)
	if len(errs) > 0 {
		Error(w, errs, http.StatusBadRequest)
		return
	}

	var user models.User
	user.ID = profile.ID
	user.Password = up.Password

	err = user.UpdatePassword(up.NewPassword)
	if err != nil {
		Error(w, err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(nil)
}
