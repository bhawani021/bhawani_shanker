package models

// TemplateView structure ...
type TemplateView struct {
	Error           interface{}
	Success         string
	Authenticated   bool
	GoogleMapAPIKey string
	ID              int64
	Email           string
	Name            string
	Address         string
	Telephone       string
}

// Profile structure ...
type Profile struct {
	ID        int64
	Email     string
	Name      string
	Address   string
	Telephone string
}
