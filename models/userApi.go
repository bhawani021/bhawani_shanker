package models

import (
	"database/sql"
	"errors"
	"strings"
	"time"

	"bitbucket.org/bhawani021/bhawani_shanker/internal"

	log "github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

var db *sql.DB
var err error

// User structure ...
type User struct {
	ID        int64     `json:"id"`
	Email     string    `json:"email"`
	Password  string    `json:"password"`
	Name      string    `json:"name"`
	Address   string    `json:"address"`
	Telephone string    `json:"telephone"`
	Token     string    `json:"token"`
	Expiry    time.Time `json:"expiry"`
	Created   time.Time `json:"created"`
	Modified  time.Time `json:"modified"`
}

// UserProfile structure ...
type UserProfile struct {
	ID        int64  `json:"id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
}

// UpdatePassword structure ...
type UpdatePassword struct {
	Password    string `json:"password"`
	NewPassword string `json:"new_password"`
}

// GoogleUserInfo structure ...
type GoogleUserInfo struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

// Login user ...
func (u User) Login(skipPasswordCheck bool) error {
	log.WithError(nil).Info("Login user.")

	// Establish database connection
	db, err = sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		log.WithError(err).Error("DB connection.")
		return err
	}
	defer db.Close()

	email := strings.TrimSpace(u.Email)

	// Grab from database
	var databasePassword string

	// Check user email in database
	qry := "SELECT password FROM user WHERE email=?"
	err = db.QueryRow(qry, email).Scan(&databasePassword)
	if err != nil {
		log.WithError(err).Error("Get password.")
		if err == sql.ErrNoRows {
			return errors.New("Email/Password is incorrect")
		}
		return err
	}

	// Validate password
	if skipPasswordCheck == false {
		err = bcrypt.CompareHashAndPassword([]byte(databasePassword), []byte(u.Password))
		if err != nil {
			return errors.New("Invalid password provided")
		}
	}

	// Update token and expiry date
	qry = "UPDATE user SET token=?, expiry=? WHERE email=?"
	_, err = db.Exec(qry, u.Token, u.Expiry, email)
	if err != nil {
		return err
	}

	return nil
}

// Save user data into mysql db
func (u User) Save() (int64, error) {
	log.WithError(nil).Info("Insert user into database.")
	var id int64

	// Establish database connection
	db, err = sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		log.WithError(err).Error("DB connection.")
		return id, err
	}
	defer db.Close()

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	email := strings.TrimSpace(u.Email)

	// Insert a new user record into database
	qry := "INSERT INTO user(name, email, password, address, telephone, token, expiry, created, modified) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)"
	req, err := db.Exec(qry, u.Name, email, hashedPassword, u.Address, u.Telephone, u.Token, u.Expiry, time.Now(), time.Now())
	if err != nil {
		return id, err
	}

	// Get primary key
	id, err = req.LastInsertId()
	if err != nil {
		return id, err
	}

	return id, nil
}

// CheckUserByEmail ...
func (u User) CheckUserByEmail() (bool, error) {
	log.WithError(nil).Info("Validate if email address already exists.")
	var id int64
	email := strings.TrimSpace(u.Email)

	// Establish database connection
	db, err := sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		log.WithError(err).Error("DB connection.")
		return false, err
	}
	defer db.Close()

	// Get user id
	qry := "SELECT id FROM user WHERE email=?"
	err = db.QueryRow(qry, email).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}

	if id > 0 {
		return true, errors.New("Email address already exists")
	}

	return false, nil
}

// GetUserProfileByToken ...
func (p UserProfile) GetUserProfileByToken(token string) (UserProfile, error) {
	log.WithError(nil).Info("Get user info using token")

	// Establish database connection
	db, err := sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		log.WithError(err).Error("DB connection")
	}
	defer db.Close()

	// Get user raw
	qry := "SELECT id, name, email, address, telephone from user WHERE token=?"
	err = db.QueryRow(qry, token).Scan(&p.ID, &p.Name, &p.Email, &p.Address, &p.Telephone)
	if err != nil {
		return p, err
	}

	// TODO: validate expiry date of token
	// if time.Now() > user.Expiry {
	// 	log.WithFields(log.Fields{
	// 		"error": err,
	// 		"token": token,
	// 	}).Error("Error get user info using token.")
	// 	return user, errors.New("Invalid token")
	// }

	return p, nil
}

// EditProfile ...
func (p UserProfile) EditProfile() error {
	log.WithError(nil).Info("Update user profile")
	emailExists := true
	// Establish database connection
	db, err := sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		log.WithError(err).Error("DB connection")
	}
	defer db.Close()

	// Validate user email
	var id int64
	qry := "SELECT id from user WHERE email=?"
	err = db.QueryRow(qry, p.Email).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			emailExists = false
		} else {
			return err
		}
	}
	if id == p.ID {
		emailExists = false
	}
	if emailExists == true {
		return errors.New("Provided email address already registered")
	}

	// Update user object
	qry = "UPDATE user SET email=?, name=?, address=?, telephone=? WHERE id=?"
	_, err = db.Exec(qry, p.Email, p.Name, p.Address, p.Telephone, p.ID)
	if err != nil {
		return err
	}
	return nil
}

// UpdatePassword ...
func (u User) UpdatePassword(newPassword string) error {
	log.WithError(nil).Info("Update user password.")

	// Establish database connection
	db, err = sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		log.WithError(err).Error("DB connection.")
		return err
	}
	defer db.Close()

	// Grab from database
	var databasePassword string

	// Check user password correct
	qry := "SELECT password FROM user WHERE id=?"
	err = db.QueryRow(qry, u.ID).Scan(&databasePassword)
	if err != nil {
		return err
	}

	// Validate password
	err = bcrypt.CompareHashAndPassword([]byte(databasePassword), []byte(u.Password))
	if err != nil {
		log.WithError(err).Error("Current password.")
		return errors.New("Current password is incorrect")
	}

	// Update token and expiry date
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	qry = "UPDATE user SET password=? WHERE id=?"
	_, err = db.Exec(qry, hashedPassword, u.ID)
	if err != nil {
		return err
	}
	return nil
}
