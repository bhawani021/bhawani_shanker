package web

import (
	"html/template"
	"net/http"

	"bitbucket.org/bhawani021/bhawani_shanker/models"
	"github.com/gorilla/securecookie"
)

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32),
)

// SetSession ...
func SetSession(key string, val string, w http.ResponseWriter) {
	value := map[string]string{
		key: val,
	}

	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(w, cookie)
	}
}

// GetSession ...
func GetSession(key string, r *http.Request) (value string) {
	if cookie, err := r.Cookie("session"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
			value = cookieValue[key]
		}
	}
	return value
}

// ClearSession ...
func ClearSession(w http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(w, cookie)
}

// ExecuteTemplate ...
func ExecuteTemplate(tempName string, view models.TemplateView, w http.ResponseWriter) {
	tmpl, _ := template.ParseFiles(tempName)
	tmpl.Execute(w, &view)
	return
}
