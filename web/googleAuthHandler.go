package web

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"bitbucket.org/bhawani021/bhawani_shanker/internal"
	"bitbucket.org/bhawani021/bhawani_shanker/models"

	log "github.com/Sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	googleAuthOauthConfig = &oauth2.Config{
		RedirectURL:  internal.WebURL + "/GoogleAuthCallback",
		ClientID:     internal.GoogleClientID,
		ClientSecret: internal.GoogleClientSecret,
		Scopes: []string{"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint: google.Endpoint,
	}
	oauthStateString   = "1abc-2jp0-7u89-99ab"
	googleAuthTemplate = "templates/google_auth_error.html"
)

// GoogleAuth handler ...
func GoogleAuth(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("Google Auth call.")
	url := googleAuthOauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

// GoogleAuthCallback handler ...
func GoogleAuthCallback(w http.ResponseWriter, r *http.Request) {
	log.WithError(nil).Info("Google Auth callback.")

	var view models.TemplateView

	// Check state param
	state := r.FormValue("state")
	if state != oauthStateString {
		view.Error = errors.New("Error found while Google authentication")
		ExecuteTemplate(googleAuthTemplate, view, w)
		return
	}

	// Get code
	code := r.FormValue("code")
	token, err := googleAuthOauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.WithError(err).Error("Read code.")
		view.Error = err
		ExecuteTemplate(googleAuthTemplate, view, w)
		return
	}

	// Call internal API for authentication
	req, err := http.NewRequest("GET", internal.API+"/user/google/auth", nil)
	if err != nil {
		log.WithError(err).Error("Call to internal Google Auth API.")
		view.Error = err
		ExecuteTemplate(googleAuthTemplate, view, w)
		return
	}
	req.Header.Set(internal.XGoogleAccessToken, token.AccessToken)

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.WithError(err).Error("Send request internal Google Auth API.")
		view.Error = err
		ExecuteTemplate(googleAuthTemplate, view, w)
		return
	}

	// Redirect to profile page if user auth is successful
	if resp.StatusCode == http.StatusOK {
		token := resp.Header.Get(internal.XUserToken)
		SetSession(internal.XUserToken, token, w)
		http.Redirect(w, r, "/user/profile", http.StatusMovedPermanently)
		return
	}

	// Read response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.WithError(err).Error("Read response data.")
		view.Error = err
		ExecuteTemplate(googleAuthTemplate, view, w)
		return
	}

	// Unmarshal response info
	err = json.Unmarshal(body, &view)
	if err != nil {
		log.WithError(err).Error("Unmarshall response data")
		ExecuteTemplate(googleAuthTemplate, view, w)
		return
	}
	ExecuteTemplate(googleAuthTemplate, view, w)
}
