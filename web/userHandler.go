package web

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"bitbucket.org/bhawani021/bhawani_shanker/internal"
	"bitbucket.org/bhawani021/bhawani_shanker/models"

	log "github.com/Sirupsen/logrus"
)

var (
	indexTemplate          = "templates/index.html"
	loginTemplate          = "templates/login.html"
	signupTemplate         = "templates/signup.html"
	profileTemplate        = "templates/profile.html"
	editProfileTemplate    = "templates/edit_profile.html"
	updatePasswordTemplate = "templates/update_password.html"
)

func getProfile(r *http.Request) (bool, models.Profile) {
	log.WithError(nil).Info("Get user info")
	var profile models.Profile

	// Check User session
	token := GetSession(internal.XUserToken, r)
	if token == "" {
		return false, profile
	}

	// Build call for user profile API
	req, err := http.NewRequest("GET", internal.API+"/user/profile", nil)
	if err != nil {
		log.WithError(err).Error("Build profile API call")
		return false, profile
	}
	req.Header.Set(internal.XUserToken, token)

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.WithError(err).Error("Do profile API call")
		return false, profile
	}

	// Check status code
	if resp.StatusCode == http.StatusOK {
		// Read resonse body
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.WithError(err).Error("Read response body")
			return false, profile
		}

		// Ummarshal response
		err = json.Unmarshal(body, &profile)
		if err != nil {
			log.WithError(err).Error("Unmarshal response")
			return false, profile
		}
		return true, profile
	}
	return false, profile
}

func auth(api string, tempName string, w http.ResponseWriter, r *http.Request) {
	var view models.TemplateView
	if r.Method == "POST" {
		email := r.FormValue("email")
		password := r.FormValue("password")

		// Build request payload
		payload := struct {
			Email    string `json:"email"`
			Password string `json:"password"`
		}{
			email,
			password,
		}

		view.Email = email

		// Marshall payload
		b, err := json.Marshal(payload)
		if err != nil {
			view.Error = []error{err}
			ExecuteTemplate(tempName, view, w)
			return
		}

		// Build call for API
		req, err := http.NewRequest("POST", api, bytes.NewBuffer(b))
		if err != nil {
			log.WithError(err).Error("Build API call")
			view.Error = []error{err}
			ExecuteTemplate(tempName, view, w)
			return
		}
		req.Header.Set("Content-Type", "application/json")

		// Send request
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.WithError(err).Error("Do API call")
			view.Error = []error{err}
			ExecuteTemplate(tempName, view, w)
			return
		}

		// Redirect to profile page if user auth is successful
		if resp.StatusCode == http.StatusOK {
			token := resp.Header.Get(internal.XUserToken)
			SetSession(internal.XUserToken, token, w)
			http.Redirect(w, r, "/user/profile", http.StatusMovedPermanently)
			return
		}

		// Read response data
		body, _ := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.WithError(err).Error("Read response data")
			view.Error = []error{err}
			ExecuteTemplate(tempName, view, w)
			return
		}

		// Unmarshal data
		err = json.Unmarshal(body, &view)
		if err != nil {
			log.WithError(err).Error("Unmarshall response data")
			view.Error = []error{err}
			ExecuteTemplate(tempName, view, w)
			return
		}
		view.Email = email
	}
	ExecuteTemplate(tempName, view, w)
}

// Index handler ...
func Index(w http.ResponseWriter, r *http.Request) {

	// Check user authentication
	auth, profile := getProfile(r)

	// Make template view
	view := models.TemplateView{
		Authenticated: auth,
		ID:            profile.ID,
		Name:          profile.Name,
		Email:         profile.Email,
		Address:       profile.Address,
		Telephone:     profile.Telephone,
	}

	// Render template
	ExecuteTemplate(indexTemplate, view, w)
}

// Login handler ...
func Login(w http.ResponseWriter, r *http.Request) {
	API := internal.API + "/user/login"
	log.WithError(nil).Info(API)
	auth(API, loginTemplate, w, r)
}

// Signup ...
func Signup(w http.ResponseWriter, r *http.Request) {
	API := internal.API + "/user/signup"
	log.WithError(nil).Info(API)
	auth(API, signupTemplate, w, r)
}

// Logout handler ...
func Logout(w http.ResponseWriter, r *http.Request) {
	ClearSession(w)
	http.Redirect(w, r, "/login", 301)
}

// Profile handler
func Profile(w http.ResponseWriter, r *http.Request) {
	// Check user authentication
	auth, profile := getProfile(r)
	if auth == false {
		http.Redirect(w, r, "/login", 301)
		return
	}

	// Make template view
	view := models.TemplateView{
		ID:        profile.ID,
		Name:      profile.Name,
		Email:     profile.Email,
		Address:   profile.Address,
		Telephone: profile.Telephone,
	}

	// Render template
	ExecuteTemplate(profileTemplate, view, w)
}

// EditProfile handler ...
func EditProfile(w http.ResponseWriter, r *http.Request) {
	// Check user authentication
	auth, profile := getProfile(r)
	if auth == false {
		http.Redirect(w, r, "/login", 301)
		return
	}
	// Make template view
	view := models.TemplateView{
		ID:              profile.ID,
		Name:            profile.Name,
		Email:           profile.Email,
		Address:         profile.Address,
		Telephone:       profile.Telephone,
		GoogleMapAPIKey: internal.GoogleMapAPIKey,
	}

	if r.Method == "POST" {
		email := r.FormValue("email")
		name := r.FormValue("name")
		address := r.FormValue("address")
		telephone := r.FormValue("telephone")

		// View data update
		view.Email = email
		view.Name = name
		view.Address = address
		view.Telephone = telephone

		// Make payload
		payload := struct {
			Email     string `json:"email"`
			Name      string `json:"name"`
			Address   string `json:"address"`
			Telephone string `json:"telephone"`
		}{
			email,
			name,
			address,
			telephone,
		}

		// Mashall payload
		b, _ := json.Marshal(payload)

		// Get Auth token
		token := GetSession(internal.XUserToken, r)

		// Build call to API
		req, _ := http.NewRequest("PUT", internal.API+"/user/profile", bytes.NewBuffer(b))
		req.Header.Set(internal.XUserToken, token)
		client := &http.Client{}
		resp, _ := client.Do(req)

		// Check status code
		if resp.StatusCode == http.StatusOK {
			view.Success = "Profile updated successfully."
			ExecuteTemplate(editProfileTemplate, view, w)
			return
		}

		// Read response data
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.WithError(err).Error("Read response data")
			view.Error = []error{err}
			ExecuteTemplate(editProfileTemplate, view, w)
			return
		}

		// Unmarshal data
		err = json.Unmarshal(body, &view)
		if err != nil {
			log.WithError(err).Error("Unmarshall response data")
			view.Error = []error{err}
			ExecuteTemplate(editProfileTemplate, view, w)
			return
		}

	}
	// Render template
	ExecuteTemplate(editProfileTemplate, view, w)
}

// UpdatePassword handler ..
func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	// Check user authentication
	auth, profile := getProfile(r)
	if auth == false {
		http.Redirect(w, r, "/login", 301)
		return
	}
	// Make template view
	view := models.TemplateView{
		ID:        profile.ID,
		Name:      profile.Name,
		Email:     profile.Email,
		Address:   profile.Address,
		Telephone: profile.Telephone,
	}

	if r.Method == "POST" {
		password := r.FormValue("password")
		newPassword := r.FormValue("new_password")

		// Make payload
		payload := struct {
			Password    string `json:"password"`
			NewPassword string `json:"new_password"`
		}{
			password,
			newPassword,
		}

		// Mashall payload
		b, _ := json.Marshal(payload)

		// Get Auth token
		token := GetSession(internal.XUserToken, r)

		// Build call to API
		req, _ := http.NewRequest("PUT", internal.API+"/user/password", bytes.NewBuffer(b))
		req.Header.Set(internal.XUserToken, token)
		client := &http.Client{}
		resp, _ := client.Do(req)

		// Check status code
		if resp.StatusCode == http.StatusOK {
			view.Success = "Password updated successfully."
			ExecuteTemplate(updatePasswordTemplate, view, w)
			return
		}

		// Read response data
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.WithError(err).Error("Read response data")
			view.Error = []error{err}
			ExecuteTemplate(updatePasswordTemplate, view, w)
			return
		}

		// Unmarshal data
		err = json.Unmarshal(body, &view)
		if err != nil {
			log.WithError(err).Error("Unmarshall response data")
			view.Error = []error{err}
			ExecuteTemplate(updatePasswordTemplate, view, w)
			return
		}

	}
	// Render template
	ExecuteTemplate(updatePasswordTemplate, view, w)
}
