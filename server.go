package main

import (
	"database/sql"
	"log"
	"net/http"

	"bitbucket.org/bhawani021/bhawani_shanker/api"
	"bitbucket.org/bhawani021/bhawani_shanker/internal"
	"bitbucket.org/bhawani021/bhawani_shanker/web"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

var (
	db  *sql.DB
	qry = `CREATE TABLE IF NOT EXISTS user (
			id int(11) NOT NULL AUTO_INCREMENT, 
			name varchar(100) DEFAULT NULL,
			email varchar(200) NOT NULL,
			password varchar(200) NOT NULL,
			address varchar(500) DEFAULT NULL,
			telephone varchar(20) DEFAULT NULL,
			token varchar(32) NOT NULL,
			expiry datetime DEFAULT NULL,
			created datetime DEFAULT NULL,
			modified datetime DEFAULT NULL,
			PRIMARY KEY (id)
		) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;`
)

func init() {
	db, err := sql.Open("mysql", internal.MysqlConnectionString)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	_, err = db.Exec(qry)
	if err != nil {
		panic(err)
	}
}

func main() {
	r := mux.NewRouter().StrictSlash(false)
	// Web
	r.HandleFunc("/", web.Index)
	r.HandleFunc("/login", web.Login)
	r.HandleFunc("/GoogleAuth", web.GoogleAuth)
	r.HandleFunc("/GoogleAuthCallback", web.GoogleAuthCallback)
	r.HandleFunc("/signup", web.Signup)
	r.HandleFunc("/logout", web.Logout)
	r.HandleFunc("/user/profile", web.Profile)
	r.HandleFunc("/user/profile/edit", web.EditProfile)
	r.HandleFunc("/user/password/update", web.UpdatePassword)

	// Api
	r.HandleFunc("/v1/user/login", api.Login).Methods("POST")
	r.HandleFunc("/v1/user/google/auth", api.UserGoogleAuth).Methods("GET")
	r.HandleFunc("/v1/user/signup", api.Signup).Methods("POST")
	r.HandleFunc("/v1/user/profile", api.Profile).Methods("GET")
	r.HandleFunc("/v1/user/profile", api.EditProfile).Methods("PUT")
	r.HandleFunc("/v1/user/password", api.ChangePassword).Methods("PUT")

	log.Fatal(http.ListenAndServe(internal.Port, r))
}
